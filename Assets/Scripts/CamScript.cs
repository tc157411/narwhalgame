﻿using UnityEngine;
using System.Collections;

public class CamScript : MonoBehaviour {

	public GameObject camTarget;

	void Update () 
	{
		Vector3 targetPos = new Vector3 (camTarget.transform.position.x + 10, camTarget.transform.position.y, camTarget.transform.position.z);
		transform.position = Vector3.Lerp (transform.position, targetPos, 1f);
	}
}
