﻿using UnityEngine;
using System.Collections;

public class IceDestroy : MonoBehaviour {


	public GameObject IceBreaker;
	public AudioClip shatter;

	void OnTriggerEnter(Collider other)
	{
		if (other.tag == "NarwhalHorn") 
		{
			Instantiate(IceBreaker, transform.position, transform.rotation);
			Destroy(gameObject);
			AudioSource.PlayClipAtPoint(shatter, transform.position);
		}
	}

}
