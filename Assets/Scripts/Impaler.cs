﻿using UnityEngine;
using System.Collections;

public class Impaler : MonoBehaviour {

	public GameObject node1;
	public GameObject node2;
	public GameObject node3;
	public int score = 0;

	public float Countdown = 0.2f;

	bool node1used;
	bool node2used;
	bool node3used;
	public bool PenguinsHere;
	bool KillAll;

	GameObject GUIObject;

	// Use this for initialization
	void Start () 
	{
		node1 = GameObject.FindGameObjectWithTag("Node1");
		node2 = GameObject.FindGameObjectWithTag("Node2");
		node3 = GameObject.FindGameObjectWithTag("Node3");
		GUIObject = GameObject.FindGameObjectWithTag("GUI");
	}
	
	void Update () 
	{
		if (KillAll) 
		{
			Countdown -= Time.deltaTime;
			if(Countdown <=0f)
			KillAll = false;	
		}
	}

	void OnTriggerEnter(Collider other)
	{
		if (other.tag == "Penguin")
		{	
			GUIObject.GetComponent<ScoreScript>().score += 1;
			PenguinsHere = true;
			if (!node1used)
			{
				other.GetComponent<PenguinMover> ().myNode = node1;
				other.GetComponent<PenguinMover> ().toBank = true;
				node1used = true;
			}
			else if (!node2used)
			{
				other.GetComponent<PenguinMover> ().myNode = node2;
				other.GetComponent<PenguinMover> ().toBank = true;
				node2used = true;
			}
			else if (!node3used)
			{
				other.GetComponent<PenguinMover> ().myNode = node3;
				other.GetComponent<PenguinMover> ().toBank = true;
				node3used = true;
			}
			else if (!node3used)
			{
				other.GetComponent<PenguinMover> ().myNode = node3;
				other.GetComponent<PenguinMover> ().toBank = true;
				node3used = true;
			}
			else if (node3used)
			{
				other.GetComponent<PenguinMover> ().myNode = node3;
				other.GetComponent<PenguinMover> ().toBank = true;
				node3used = true;
			}
		}
		if (other.tag == "Hazard" && PenguinsHere == true) 
		{
			KillAll = true;
			node1used = false;
			node2used = false;
			node3used = false;
			score -= 3;
			PenguinsHere = false;
		}
		else if (other.tag == "Hazard" && PenguinsHere == false)
		{
			Application.LoadLevel(Application.loadedLevel);
		}
	}

	void OnTriggerStay(Collider other)
	{
		if (other.tag == "Penguin" && KillAll == true)
		{
			Destroy(other.gameObject);
		}
	}
}
