﻿using UnityEngine;
using System.Collections;

public class NarwhalMove : MonoBehaviour
{

		public bool inWater = false;
		public float turnSpeed = 1.0f;
		public float slowerTurnSpeed = 0.5f;
		public float waterSpeed = 10.0f;
		public float landSpeed = 5.0f;
		public float maxRotationUp = 160.0f;
		public float maxRotationDown = 20.0f;
		public float rotationZ = 90.0f;
		public float maxSpeed = 100.0f;
		public float boostSpeed = 200.0f;
		public Vector3 currentSpeed;
		public float slowerTurnAngleUp = 140.0f;
		public float slowerTurnAngleDown = 40.0f;
		public Vector3 waterGravity = new Vector3 (0, 3, 0);
		public Vector3 landGravity = new Vector3 (0, -9, 0);
		public bool onLand = false;
		public float flopHeight = 5.0f;
		public GameObject IceFX;


		void Update ()
		{

			if (Input.GetButtonDown ("Jump")) 
			{
				if (onLand == true) 
				{
					rigidbody.AddForce (0, flopHeight, 0);
				}
			}
		}

		void FixedUpdate ()
		{
			if (inWater == true) 
			{
				Physics.gravity = waterGravity;
			}
			else 
			{
				Physics.gravity = landGravity;
			}
			
			if (inWater == true) 
			{
				rigidbody.AddRelativeForce (waterSpeed, 0, 0);

				if (rigidbody.velocity.magnitude > maxSpeed) 
				{
					rigidbody.AddForce (-rigidbody.velocity.normalized * (rigidbody.velocity.magnitude - maxSpeed), ForceMode.VelocityChange);
				}
			}

			if (Input.GetKey (KeyCode.A)) 
			{
				if (rotationZ < slowerTurnAngleDown) 
				{
					rotationZ -= slowerTurnSpeed;
				}
				else 
				{
					rotationZ -= turnSpeed;
				}
			}

			if (Input.GetKey (KeyCode.D)) 
			{
				if (rotationZ > slowerTurnAngleUp) 
				{
					rotationZ += slowerTurnSpeed;
				} 
				else 
				{
					rotationZ += turnSpeed;
				}


		}

		if (Input.GetKey (KeyCode.R)) 
		{
			Application.LoadLevel(Application.loadedLevel);
		
		}


			rotationZ = Mathf.Clamp (rotationZ, maxRotationDown, maxRotationUp);

			transform.localEulerAngles = new Vector3 (transform.localEulerAngles.x, transform.localEulerAngles.y, -rotationZ);

		}


		void OnTriggerEnter (Collider other)
		{
			if (other.tag == "Ice") 
			{
				onLand = true;	
				IceFX.SetActive(true);
				Debug.Log ("I can Flop!!");
				audio.Play();
			}

			if (other.tag == "SpeedBoost") {

			rigidbody.AddRelativeForce (boostSpeed, 0, 0);

				}

		if (other.tag == "Goal") {

			Application.LoadLevel("GameEnd");

				}
		}
	
		void OnTriggerExit (Collider other)
		{
			if (other.tag == "Ice") 
			{
				onLand = false;
				audio.Stop();				
				IceFX.SetActive(false);
			}
		}
}