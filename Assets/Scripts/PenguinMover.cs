﻿using UnityEngine;
using System.Collections;

public class PenguinMover : MonoBehaviour {

	public GameObject myNode;
	public GameObject Blood;
	public bool toBank;
	public AudioClip splat;
	public GameObject alive;
	public GameObject dead;



	void Start () 
	{
	}
	
	void Update () 
	{
		if (myNode == null)
		{
			Debug.Log("I have no node");
		}
		else
		{
			transform.position = myNode.transform.position;
			transform.rotation = myNode.transform.rotation;
		}
	}

	void OnTriggerEnter(Collider other)
	{
		if(other.tag == "NarwhalHorn")
		{
			Blood.SetActive(true);
			dead.SetActive (true);
			alive.SetActive (false);
			AudioSource.PlayClipAtPoint(splat, transform.position);
		}
	}
}
