﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class ScoreScript : MonoBehaviour {

	public int score = 0;
	Text text;

	void Start () 
	{
		text = GetComponent <Text> ();
		score = 0;
	}
	
	void Update () 
	{
		text.text = "Penguin Friends!: " + score.ToString();
	}
}
