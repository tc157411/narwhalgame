﻿using UnityEngine;
using System.Collections;

public class Water : MonoBehaviour {

	public AudioClip splash;
	public GameObject splashFX;


	void OnTriggerEnter(Collider other)
	{
		if (other.tag == "Narwhal") 
		{
			other.GetComponent<NarwhalMove> ().inWater = true;
			AudioSource.PlayClipAtPoint(splash, transform.position);				
			Instantiate(splashFX, other.transform.position, transform.rotation);
		}
	}

	void OnTriggerExit(Collider other)
	{
		if (other.tag == "Narwhal") 
		{
			other.GetComponent<NarwhalMove> ().inWater = false;
		}
	}

}
